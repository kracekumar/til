---
title: "Markdown Backticks"
date: 2021-08-22T14:30:47+05:30
draft: false
Tags: ["markdown"]
---

In markdown spec, content between triple back ticks ``` is a code fence/code block. 

```python
var = "this is a python variable"
```

According to [GFM](Github Flavored Markdown) and [this blog post](https://koaning.io/til/2021-08-20-markdown-ticks/), it's possible to have more than three backpacks as long followed by code and equal number of backticks. It's valid to have four backticks and five backticks. What's the use case?

````markdown

### Markdown content

```python

print("Hell of a boring life")

```
### Closing thoughts


````

From GFM Spec

> The closing code fence must be at least as long as the opening fence: 

[Source](https://github.github.com/gfm/#example-94)

- The outer block can be one type and inner codeblock can be another type. Say, outer block can be markdown and inner block can be `Python`.



Preview from https://dillinger.io/

![](/images/markdown/markdown_preview_4_backticks.png)

Preview from Github

![](/images/markdown/gh_preview.png)

### References

- [GFM Specs](https://github.github.com/gfm/#example-94)
- [TIL Koaning Markdown tricks](https://koaning.io/til/2021-08-20-markdown-ticks/)

