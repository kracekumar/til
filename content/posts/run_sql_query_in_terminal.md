---
title: "Run SQL Query In Terminal"
date: 2021-08-21T12:38:25+05:30
draft: false
---

To run a SQL query, you use CLI(psql) or GUI(pgadmin/metabase) program to execute the query. Sometimes, you want to run the command to look a table structure and number of records in the table without worrying about opening the client. So rather than running the CLI client, you can pass the query string to `psql` command with the flag, `-c`.

The command looks like 

```sql
$psql -h localhost -d typeboard -c "select count(*) from project"
 count
-------
     1
(1 row)
```

 

![](/images/psql_query/psql_query.png)
