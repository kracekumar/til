---
title: "TLDR"
date: 2021-08-21T11:47:18+05:30
draft: false
---

[Man](https://linux.die.net/man/1/grep) pages in terminal provides manual about a command. There is no usage/examples in the document. This was deliberate, according to [wise_steve](https://twitter.com/wise_steve/status/1427310230174638082). 

[Julia Evans asked the question in the twitter](https://twitter.com/b0rk/status/1427308140916363269)

> Q: does anyone know why man pages (like the grep man page https://linux.die.net/man/1/grep) often don't have an examples section with the most common ways you'd want to use it?

[Wise Steve replied in the twitter](https://twitter.com/wise_steve/status/1427310230174638082) 

> A: Part of the reason is likely that the “Linux” man pages are GNU man pages, and the GNU-inclined believe that examples should be in the texinfo docs, not the short man page.

Sometimes, all you want is to find out what switch to pass to make the search case-insenstive or what flag to pass to print the usage in GB.

 [TLDR](https://tldr.sh/) is a Collaborative cheatsheets for console commands. 

![](/images/tldr/tldr_du.png)

[The displayed content is a markdown file](https://github.com/tldr-pages/tldr/blob/main/pages/common/du.md).



### References

- Initial Question - https://twitter.com/b0rk/status/1427308140916363269
- Response - https://twitter.com/wise_steve/status/1427310230174638082
- TLDR Website - https://tldr.sh/
- Du cheatsheet - https://github.com/tldr-pages/tldr/blob/main/pages/common/du.md
- Man Page - https://linux.die.net/man/1/grep
