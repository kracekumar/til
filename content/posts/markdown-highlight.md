---
title: "Markdown Highlight"
date: 2021-09-12T02:51:16+05:30
draft: false
Tags: ["markdown"]
---

Markdown supports HTML 5 tags inside it. To highlight a piece of text(yellow color background), you can use [mark HTML tag](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/mark) `<mark> text </mark>` or use `==highlight==` (may not be supported everywhere). 

```html
<mark>
**Note**: All the screenshots are taken from the metabase Mac App, 
the interface and menus may look different in web UI.
</mark>

```



The output of the tag in the mkdocs generated file.

![highlighted text in yellow](/images/markdown-highlighting/highlight.png)

### References

- Mark tag - https://developer.mozilla.org/en-US/docs/Web/HTML/Element/mark
- Stackoverflow answer - https://stackoverflow.com/questions/25104738/text-highlight-in-markdown
