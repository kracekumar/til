---
title: "New function in builtins"
date: 2021-11-01T23:30:55+05:30
draft: false
---

## Accessing Builtins

Python builtins module contains [functions](https://docs.python.org/3/library/functions.html#built-in-funcs) and [constants](https://docs.python.org/3/library/constants.html#built-in-consts). These functions and constants are available in the interpreter and can be accessed via their name.

```python
>>> max
<built-in function max>
>>> __debug__
True
>>> Ellipsis
Ellipsis
>>>
```

Buitlins can be accessed via `__builtins__` variable.

```python
>>> __builtins__.max
<built-in function max>
>>> __builtins__.Ellipsis
Ellipsis
```

You can also access bulitins from `builtins` module.

```python
>>> import builtins
>>> builtins.max
<built-in function max>
>>> builtins.Ellipsis
Ellipsi
```



## Setting functions and variables in builtins

Python import system is executed only once. A variable or reference to a name can be set to buitlins module and can be accessed afterwards. For example, you can add reference to a function `palindrome` and can be accessed later in the program.

Here is a simple example.

-  `first.py` defines a function `palindrome`.
- `first.py` assigns the function reference `palindrome to `the builtins module.
- `first.py` imports `is_palindrome`  from the `second.py` file.
- `first.py` calls `is_palindrome` function.
- `is_palindrome` function internally calls `palindrome` function.

```python
# first.py 
def palindrome(name: str) -> bool:
    return name[::-1] == name


__builtins__.palindrome = palindrome

def greet():
    pass

from second import is_palindrome

print("is malayalam palindrome:", is_palindrome("malayalam"))
print("is tamil palindrome:", is_palindrome("tamil"))

```



```python
# second.py
def is_palindrome(name: str) -> bool:
    return palindrome(name)

try:
    print(greet())
except NameError:
    print('greet function is not visible')

```



```bash
$ python3 first.py 
greet function is not visible
is malayalam palindrome: True
is tamil palindrome: False
```



For example, `ipython` shell sets `display` function in the IPython shell. [Source](https://github.com/ipython/ipython/blob/1eeb3a47f9772c6847439588101bec4b6dfa3656/IPython/core/interactiveshell.py)



<blockquote class="twitter-tweet" data-dnt="true" data-theme="dark"><p lang="en" dir="ltr"><a href="https://twitter.com/hashtag/TIL?src=hash&amp;ref_src=twsrc%5Etfw">#TIL</a> You can assign a value or reference in builtins Python module to a name and access it later in the program directly by the name. <a href="https://t.co/syogx1ZMc9">https://t.co/syogx1ZMc9</a> <a href="https://t.co/9Tkmnl5f23">pic.twitter.com/9Tkmnl5f23</a></p>&mdash; kracekumar || கிரேஸ்குமார் (@kracetheking) <a href="https://twitter.com/kracetheking/status/1455244331086802944?ref_src=twsrc%5Etfw">November 1, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

