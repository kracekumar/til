---
title: "Set Syntax"
date: 2021-08-07T12:59:13+05:30
draft: false
tags: ["python"]
---

Python use symbols to initialize variable.
`[]` creates a new empty list, `()` creates a new tuple,
`{}` creates a new empty dictionary. These are well documented.

`{*()}` creates a new set. The syntax to create a new set is
`set(<iter>)`. The starred expression, `*()` unpacks the value into the set.


### Code Examples

``` python-console
In [1]: {}
Out[1]: {}

In [2]: []
Out[2]: []

In [3]: type([])
Out[3]: list

In [4]: type({})
Out[4]: dict

In [5]: {*()}
Out[5]: set()

In [6]: type({*()})
Out[6]: set

In [7]: {*(1, 1)}
Out[7]: {1}

In [8]: {*[1, 1]}
Out[8]: {1}

In [9]: {*{1, 1}}
Out[9]: {1}

In [10]: *[1, 2]
  File "<ipython-input-10-7825c5ac490d>", line 1
    *[1, 2]
    ^
SyntaxError: can't use starred expression here
```
