---
title: "Clear Command"
date: 2021-08-08T12:59:13+05:30
draft: false
tags: ["CLI", "Emacs", "GNU/Linux"]
---

[Clear](https://man7.org/linux/man-pages/man1/clear.1.html)
command clear the terminal output. The same command fails inside eshell.

From eshell webpage

> Eshell is a shell-like command interpreter implemented in Emacs Lisp. It
invokes no external processes except for those requested by the user. It is
intended to be an alternative to the IELM (see Emacs Lisp Interaction in
The Emacs Editor) REPL for Emacs and with an interface similar to
command shells such as bash, zsh, rc, or 4dos.

`clear` command rather than clearing the screen, it takes the input cursor to the end of the buffer.

After running `clear command`

![](/images/clear/clear_end.png)

Before running `clear 1` command <br/>

![](/images/clear/clear_end_type.png)

After Clear command
![](/images/clear/clear_1_start.png)

To clear eshell, use, `clear 1` over clear command.

### References

1. Clear Command: https://man7.org/linux/man-pages/man1/clear.1.html
